import os
import sys

def setup(sphinx):
    cur_path = os.path.realpath(__file__)
    sys.path.insert(0, os.path.join(cur_path, 'sphinxcontrib'))
    from ChaiDomain import ChaiDomain
    sphinx.add_domain(ChaiDomain)

